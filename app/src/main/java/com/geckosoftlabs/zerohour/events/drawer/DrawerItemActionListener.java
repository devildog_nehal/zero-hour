package com.geckosoftlabs.zerohour.events.drawer;

public interface DrawerItemActionListener {

    void onItemClicked(String type);

}
