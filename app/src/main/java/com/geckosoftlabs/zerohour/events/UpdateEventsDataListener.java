package com.geckosoftlabs.zerohour.events;

public interface UpdateEventsDataListener {

    void onSortUpdate(int type);

    void onIsAscUpdate(boolean isAsc);

    void onEventDelete();

}
