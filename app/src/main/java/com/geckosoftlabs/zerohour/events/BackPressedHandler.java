package com.geckosoftlabs.zerohour.events;

public interface BackPressedHandler {
    void setSelectedFragment(EventsFragment fragment);
}
