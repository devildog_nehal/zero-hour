package com.geckosoftlabs.zerohour.events.item;

public interface EventItemActionListener {

    void onItemClicked(String eventId);
}
