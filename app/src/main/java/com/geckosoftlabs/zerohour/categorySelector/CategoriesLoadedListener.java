package com.geckosoftlabs.zerohour.categorySelector;


import com.geckosoftlabs.zerohour.data.Category;

import java.util.List;

public interface CategoriesLoadedListener {

    void onLoadedFinished(List<Category> categories);

}
