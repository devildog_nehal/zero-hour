package com.geckosoftlabs.zerohour.categorySelector;

public interface CategoryItemActionListener {

    void onItemClicked(String category);

}
