package com.geckosoftlabs.zerohour.editEvent;

public interface EditActionListener {

    void onCheckBoxClicked();

    void onTipsClicked();

    void onStartDateClicked();

    void onDueDateClicked();

    void onCategoryClicked();

    void onPriorityClicked();

    void onReminderClicked();

    void onFabClicked();

}
