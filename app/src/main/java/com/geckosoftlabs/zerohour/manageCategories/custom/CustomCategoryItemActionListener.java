package com.geckosoftlabs.zerohour.manageCategories.custom;

public interface CustomCategoryItemActionListener {

    void onItemClicked(String id);

}
