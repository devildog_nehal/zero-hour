package com.geckosoftlabs.zerohour.settings;

public interface SettingItemActionListener {

    void onStartOfWeekClicked();

    void onDailyReminderClicked();

    void onQuickViewBehaviorClicked();

}
