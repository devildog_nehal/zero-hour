package com.geckosoftlabs.zerohour.data;

import androidx.annotation.NonNull;

import java.util.List;

public interface CategoryDataSource {

    interface LoadCategoriesCallback {

        void onCategoriesLoaded(List<Category> categories);

        void onDataNotAvailable();

    }

    interface GetCategoryCallback {

        void onCategoryLoaded(Category category);

        void onDataNotAvailable();

    }

    void getAllCategories(@NonNull LoadCategoriesCallback callback);

    void getCategory(@NonNull String categoryId, @NonNull GetCategoryCallback callback);

    void saveCategory(@NonNull Category category);

    void deleteAllCategories();

    void deleteCategory(@NonNull String categoryId);


}
