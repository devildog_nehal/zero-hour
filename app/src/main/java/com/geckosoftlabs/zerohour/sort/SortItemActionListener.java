package com.geckosoftlabs.zerohour.sort;

public interface SortItemActionListener {

    void onItemClicked(int type);

    void onSwitchedSortOrder(boolean isAsc);

}
