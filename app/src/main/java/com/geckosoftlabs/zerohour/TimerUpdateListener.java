package com.geckosoftlabs.zerohour;

public interface TimerUpdateListener {

    void onUpdateView(long duration);
}
